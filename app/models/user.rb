class User < ActiveRecord::Base
 
  attr_accessible :address, :city, :email, :firstname, :lastname, :password, :postalcode,
   :state, :password_confirmation, :STATE_CODE
  
  #email_regex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i
  
  validates_presence_of :state, :message => 'Please select a state.'
  validates_presence_of :address, :message => 'Please provide an address.'
  validates_presence_of :city, :message => 'Please provide a city.'
  validates_presence_of :firstname, :message => 'Please provide a first name.'
  validates_presence_of :lastname, :message => 'Please provide a last name.'
  validates_format_of :postalcode, :with => /\d{5}/, :message => 'Need to provide a vaild zip code.'
  
  acts_as_authentic do |config|
    config.login_field = :email
    config.validate_email_field = false
  
  end
  
  STATE_CODE = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY',
    'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK',
     'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY']
  
end
