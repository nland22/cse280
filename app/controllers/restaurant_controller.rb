require 'factual'
require 'lazy_high_charts'

class RestaurantController < ApplicationController
  before_filter :require_user, :except => [:preview]
  #rows per page in query
  ROW_COUNT = 50 

  def index
    
    #factual api query call for rocherster hills, mi with no delivery
    factual = Factual.new("oDzUB7JRjHoE3GcH84b9Y9ymxEL3M59dlqzwvuYg", "WhsHS2otjOdxryWT2OhdZMPptZpEhHOXfuCWU4Dl")
    @query = factual.table("restaurants").filters( :country => "us", :region => "mi", :locality => "rochester hills", :meal_deliver => false)
    @query = @query.select(:name, :address, :cuisine, :price, :rating ) 
    
    #data structure values
    @page_count = ( Float(@query.total_count) / Float(ROW_COUNT) ).ceil
    @rows_last_page = @query.total_count -  (@page_count - 1) * ROW_COUNT
    
    #visualizes query in different categories
    create_graph
    
  end

  def preview
  end
  
  private
  def create_graph
    
    @hData = {} #hash for extracted query data
    
    #chart titles and labels
    gTitle = {'cuisine' => 'Restaurant Cuisine Types','address' => 'Restaurant Locations in Rochester Hills','rating' => 'Restaurant Star Rating','price' => 'Restaurant Average Meal Price' }
    gxAxis = {'cuisine' => 'Cuisine Types','address' => 'Location by Road','rating' => 'Average Number of Stars','price' => 'Pricing Range' }
    
    extractallpages
    
    #orginizes data alphabetically or numberically
    gData = @hData.sort_by {|key, value| key }
   
    @hGraph = LazyHighCharts::HighChart.new('column') do |f|
    
    #set data and names data in graph
    gData.each do |key, value|
      f.series(:name=>key ,:data=> [value])
    end
    
    #chart options
    f.title({ :text=> gTitle[params[:graph_categories] ] })  
    f.options[:chart][:defaultSeriesType] = "column" 
    #f.options[:chart][:height] = 600;
    #f.options[:chart][:width] = 800;
    
    #legend options
    f.options[:legend][:layout] = "horizontal"
    
    #xAxis options
    f.options[:xAxis][:title] = {:text => gxAxis[params[:graph_categories]] }
    f.options[:xAxis][:labels] = {:enabled => false }
    f.options[:xAxis][:minorTickLength] = 0
    f.options[:xAxis][:tickLength] = 0
    
    #yAxis Options
    f.options[:yAxis][:title] = {:text => 'Number of Restaurants' }
    f.options[:yAxis][:allowDecimals] = false
    
    
    end
    
  end #create_graph
  
  def extractallpages
  
    for page in 1..@page_count
      if page < @page_count
        @query = @query.page(page, :per => ROW_COUNT)
        extractdataby(params[:graph_categories], false)
          
      else
        @query = @query.page(page, :per => @rows_last_page)
        extractdataby(params[:graph_categories], true)
      end

    end
    
  end #extractallpages
  
   def extractdataby(category, islastpage)

    #apply default category if no category is given
    if category == nil
      params[:graph_categories] = category = 'cuisine'
    end
    
    #determines if the query is on the last page
    #and how many rows should be cycled through
    if islastpage == true
      row_count = @rows_last_page - 1
    else
      row_count = ROW_COUNT - 1
    end
  
  
      for entry in 0..row_count do
      data = []
      
      if @query[entry][category] != nil
       
       #determine how to extract data and format accordingly
        if category == 'cuisine'
           @query[entry][category].each do |str|
             data.push str
           end
    
        else
          data.push @query[entry][category].to_s
        
            if category == 'price'

            data.each do |d|
              case d
              when "1" then d.replace '$0-15'
              when "2" then d.replace '$15-30'
              when "3" then d.replace '$30-50'
              when "4" then d.replace '$50-75'
              when "5" then d.replace  '$75+'
              end
            end

          elsif category == 'address'

            data.each do |d|
              d.gsub!(/^\d*/, '')
            end

          end #end if
        
        end
      end #check nil
   

      #creates main hash table for graphing data
      data.each do |d|
        if @hData.has_key? d
        @hData[d.to_s] = @hData[d.to_s] + 1
        else
          @hData.merge!( {d.to_s => 1} )
        end
      end

    end #query

  end #sortdataby
  
  
end
